package com.oshicuru0.services;

import com.oshicuru0.dao.UserHandler;
import com.oshicuru0.model.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/user")
public class UserService {

    @GET
    @Path("/users")
    public List<User> getAllUsers() throws Exception {
        return new UserHandler().getAllUsers();
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response setUser(
            @FormParam("name") String name,
            @FormParam("surname") String surname,
            @FormParam("email") String email,
            @FormParam("phone_number") String phone_number) throws Exception {
        new UserHandler().insertUser(name,surname,email,phone_number);
        return Response.status(201).build();
    }
    @DELETE
    @Path("/{id}")
    public Response deleteUser(@PathParam("id") Integer id) throws Exception {
        new UserHandler().deleteUser(id);
        return Response.status(201).build();
    }

    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updateUser(
            @FormParam("id") int id,
            @FormParam("name") String name,
            @FormParam("surname") String surname,
            @FormParam("email") String email,
            @FormParam("phone_number") String phone_number) throws Exception {
        new UserHandler().updateUser(id,name,surname,email,phone_number);
        return Response.status(201).build();
    }
}