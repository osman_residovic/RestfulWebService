package com.oshicuru0.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {

    private final String DATABASE_NAME = "rest_database";
    private final String USERNAME = "root";
    private final String PASSWORD = "root";
    private final String DATABASE_URL = "jdbc:mysql://localhost:3306/";
    private final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";

    public Connection getConnection() throws Exception {
        try {
            Connection connection = null;
            String connectionURL = DATABASE_URL + DATABASE_NAME;
            Class.forName(DATABASE_DRIVER).newInstance();
            connection = DriverManager.getConnection(connectionURL, USERNAME, PASSWORD);
            return connection;
        } catch (SQLException e) {
            throw e;
        }
    }
}