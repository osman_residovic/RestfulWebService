package com.oshicuru0.dao;


import com.oshicuru0.model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class UserHandler {


    public ArrayList<User> getAllUsers() throws Exception {
        ArrayList<User> userList = new ArrayList<User>();
        try {
            PreparedStatement ps = new DbConnection().getConnection().prepareStatement("SELECT * FROM user");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getLong("id"));
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setEmail(rs.getString("email"));
                user.setPhone_number(rs.getString("phonenumber"));
                userList.add(user);
            }
            return userList;
        } catch (Exception e) {
            throw e;
        }
    }

    public void insertUser(String name_, String surname_, String email_, String phone_number_) throws Exception {
        String query = "INSERT INTO user (name, surname, email, phonenumber)" + "VALUES (?, ?, ?, ?)";
        PreparedStatement ps = new DbConnection().getConnection().prepareStatement(query);
        ps.setString(1, name_);
        ps.setString(2, surname_);
        ps.setString(3, email_);
        ps.setString(4, phone_number_);
        ps.execute();
    }

    public void deleteUser(Integer id) throws Exception {
        String deleteSQL = "DELETE FROM user WHERE id = ?";
        PreparedStatement preparedStatement = new DbConnection().getConnection().prepareStatement(deleteSQL);
        preparedStatement.setInt(1, id);
        preparedStatement.executeUpdate();
    }

    public void updateUser(int id, String name_, String surname_, String email_, String phone_number_) throws Exception {
        String updateTableSQL = "UPDATE user SET name = ?, surname = ?, email = ?, phonenumber = ? WHERE id = ?";
        PreparedStatement preparedStatement = new DbConnection().getConnection().prepareStatement(updateTableSQL);
        preparedStatement.setString(1, name_);
        preparedStatement.setString(2, surname_);
        preparedStatement.setString(3, email_);
        preparedStatement.setString(4, phone_number_);
        preparedStatement.setInt(5, id);
        preparedStatement.executeUpdate();
    }
}